import bpy

from .Models import ModifiersSetup
from .views import AddonPreference, Pannel, PieModifier
from .Controllers import PresetModifier, ToolsModifiers

bl_info = {
    "name": "Advanced Modifiers",
    "description": "Various tools to optimised Modifier use.",
    "author": "stilobique",
    "version": (0, 0, 3),
    "blender": (2, 80, 0),
    "location": "Panel Modifier and Pie menu viewport",
    "warning": "Beta",  # used for warning icon and text in addons panel
    "wiki_url": "https://gitlab.com/stilobique-blender-addons/Advanced-Modifiers/wikis/home",
    "tracker_url": "https://gitlab.com/stilobique-blender-addons/Advanced-Modifiers/issues",
    "support": "COMMUNITY",
    "category": "Modifiers"
}


classes = [
    # Models,
    ModifiersSetup.AngleEdge,
    ModifiersSetup.ModifierSettings,
    ModifiersSetup.ModifierSubsurf,
    # Views,
    AddonPreference.ADVANDEDMODIFIER_PT_setting,
    Pannel.MODIFIERS_PT_Tools,
    PieModifier.MENU_MT_PieModifiers,
    # Controllers,
    PresetModifier.STILO_OT_subsurf_preset,
    PresetModifier.STILO_OT_simple_deform_preset,
    PresetModifier.STILO_OT_mirror_preset,
    PresetModifier.STILO_OT_decimate_preset,
    PresetModifier.STILO_OT_displace_noise_preset,
    PresetModifier.STILO_OT_edge_split_preset,
    ToolsModifiers.STILO_OT_batch_modifiers,
    ToolsModifiers.STILO_OT_display_modifiers,
]

addon_keymaps = []


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    # Declare all scene blender variable
    bpy.types.Scene.md_vars = bpy.props.PointerProperty(type=ModifiersSetup.ModifierSettings)
    bpy.types.Scene.md_subsurf = bpy.props.PointerProperty(type=ModifiersSetup.ModifierSubsurf)
    bpy.types.Scene.md_split = bpy.props.PointerProperty(type=ModifiersSetup.AngleEdge)

    # Add panel on modifier view
    bpy.types.DATA_PT_modifiers.prepend(Pannel.MODIFIERS_PT_Preset)

    # Keyboard Mapping
    wm = bpy.context.window_manager
    if wm.keyconfigs.addon:
        km = wm.keyconfigs.addon.keymaps.new(name='Window', space_type='EMPTY')
        kmi = km.keymap_items.new(idname='wm.call_menu_pie', type='M', value='PRESS', ctrl=True, shift=True)
        kmi.properties.name = 'MENU_MT_PieModifiers'
        addon_keymaps.append((km, kmi))


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)

    del bpy.types.Scene.md_vars
    del bpy.types.Scene.md_subsurf
    del bpy.types.Scene.md_split
    bpy.types.DATA_PT_modifiers.remove(Pannel.MODIFIERS_PT_Preset)

    wm = bpy.context.window_manager
    kc = wm.keyconfigs.addon
    if kc:
        for km, kmi in addon_keymaps:
            km.keymap_items.remove(kmi)
    addon_keymaps.clear()
