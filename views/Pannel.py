import bpy


def MODIFIERS_PT_Preset(self, context):
    """
    Modifier preset, override the base UI
    """

    preferences = context.preferences
    addon_prefs = preferences.addons["Advanced-Modifiers"].preferences

    data = context.scene.md_subsurf
    layout = self.layout

    split = layout.split()
    col = split.column(align=True)
    if addon_prefs.subsurf_boolean:
        col.operator("modifiers.subsurf_preset",
                     text="Subsurf", icon="MOD_SUBSURF")
        row = col.row(align=True)
        row.prop(data, "subsurf_intensity", expand=True, toggle=True)

    col = split.column()
    if addon_prefs.noise_boolean:
        col.operator("modifiers.noise_preset", text="Noise",
                     icon="MOD_DISPLACE")

    if addon_prefs.lod_boolean:
        col.operator("modifiers.decimate_preset", text="LoD Generate",
                     icon="MOD_DECIM")

    data = context.scene.md_vars
    split = layout.split()
    col = split.column(align=True)
    if addon_prefs.mirror_boolean:
        mirror = "modifiers.mirror_preset"
        col.operator(mirror, text="Mirror", icon="MOD_MIRROR")
        row = col.split(align=True)
        row.prop(data, "axis_x", text="X", toggle=True)
        row.prop(data, "axis_y", text="Y", toggle=True)
        row.prop(data, "axis_z", text="Z", toggle=True)

    col = split.column()
    row = col.row(align=True)
    if addon_prefs.bend_boolean:
        ops = "modifiers.simpledeform_preset"
        icon = 'MOD_SIMPLEDEFORM'
        row.operator(ops, text="Twist", icon=icon).method = 'TWIST'
        icon = 'MOD_SIMPLEDEFORM'
        row.operator(ops, text="Bend", icon=icon).method = 'BEND'

    row = col.row(align=True)
    data = context.scene.md_split
    ops = "modifiers.edgesplit_preset"
    row.operator(ops, text="", icon="MOD_EDGESPLIT")
    row.prop(data, "angle_intensity", text='Edge Split angle', slider=True)


class MODIFIERS_PT_Tools(bpy.types.Panel):
    bl_label = "Modifier Tools"
    bl_idname = "MODIFIERS_PT_Tools"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "modifier"

    def draw(self, context):
        layout = self.layout
        
        row = layout.row()
        col = row.split(align=True)
        ops = 'modifiers.batch_modifiers'
        msg = 'Apply modifier(s)'
        col.operator(ops, text=msg, icon='ADD').batch = 'Apply'
        msg = 'Clear all modifier(s)'
        col.operator(ops, text=msg, icon='PANEL_CLOSE').batch = 'Remove'
        if context.scene.md_vars.expand:
            icon = 'TRIA_RIGHT'
            msg = 'Expand'
        else:
            icon = 'TRIA_DOWN'
            msg = 'Collapse'
        row.operator(ops, text=msg, icon=icon).batch = 'Expand'

        row = layout.row()
        data = context.scene.md_vars
        split = row.split(align=True)
        split.prop(data, 'md_render', icon='RESTRICT_RENDER_OFF',
                   icon_only=True)
        split.prop(data, 'md_viewport', icon='RESTRICT_VIEW_OFF',
                   icon_only=True)
        split.prop(data, 'md_edit', icon='EDITMODE_HLT', icon_only=True)
        split.prop(data, 'md_cage', icon='MESH_DATA', icon_only=True)
