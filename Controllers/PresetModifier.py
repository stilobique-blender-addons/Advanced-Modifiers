import bpy


class STILO_OT_subsurf_preset(bpy.types.Operator):
    """Apply a Subsurf modifier with an iteration value"""
    bl_idname = 'modifiers.subsurf_preset'
    bl_label = 'Subsurf modifier with a preset'
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        selected = context.active_object
        return selected is not None and selected.type == 'MESH'

    def execute(self, context):
        modifier_name = 'Subsurf'
        
        iteration = context.scene.md_subsurf

        md = context.object.modifiers
        md.new(modifier_name, type='SUBSURF')
        md[-1].levels = int(iteration.subsurf_intensity[0])
        md[-1].show_only_control_edges = True

        return {'FINISHED'}


class STILO_OT_simple_deform_preset(bpy.types.Operator):
    """Generate a simple deform modifier"""
    bl_idname = 'modifiers.simpledeform_preset'
    bl_label = 'Preset Simple Deform'
    bl_options = {'REGISTER', 'UNDO'}

    # Variables
    method: bpy.props.StringProperty(name="")

    @classmethod
    def poll(cls, context):
        selected = context.active_object
        return selected is not None and selected.type == 'MESH'

    def execute(self, context):
        # Variable pour la function
        modifier_name = 'Deform'
        context.object.modifiers.new(modifier_name, type='SIMPLE_DEFORM')

        if self.method == 'BEND':
            context.object.modifiers[-1].deform_method = 'BEND'
        elif self.method == 'TWIST':
            context.object.modifiers[-1].deform_method = 'TWIST'
        elif self.method == 'TWIST':
            context.object.modifiers[-1].deform_method = 'TAPER'
        elif self.method == 'TWIST':
            context.object.modifiers[-1].deform_method = 'STRETCH'

        return {'FINISHED'}


class STILO_OT_mirror_preset(bpy.types.Operator):
    """This operator  make add a mirror modifier for the selected object."""
    bl_idname = 'modifiers.mirror_preset'
    bl_label = 'Preset Mirror'
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        selected = context.active_object
        return selected is not None and selected.type == 'MESH'

    def execute(self, context):
        context.object.modifiers.new('mirror', type='MIRROR')
        mirror = context.object
        axis = context.scene.md_vars

        mirror.modifiers['mirror'].use_axis[0] = axis.axis_x
        mirror.modifiers['mirror'].use_axis[1] = axis.axis_y
        mirror.modifiers['mirror'].use_axis[2] = axis.axis_z
        mirror.modifiers['mirror'].use_clip = True

        return {'FINISHED'}


# TODO Preset a faire
class STILO_OT_decimate_preset(bpy.types.Operator):
    """Add a decimate modifier preset"""
    bl_idname = 'modifiers.decimate_preset'
    bl_label = 'Preset decimate'
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        selected = context.active_object
        return selected is not None and selected.type == 'MESH'
    
    def execute(self, context):
        import LoDsSettings
        ratio = LoDsSettings.variable_ratio
        
        context = bpy.context.object
        
        context.modifiers.new('lod', type='DECIMATE')
        context.modifiers['lod'].ratio = ratio
        context.modifiers['lod'].use_collapse_triangulate = True

        return {'FINISHED'}


class STILO_OT_displace_noise_preset(bpy.types.Operator):
    """This preset modifier add a noise displace"""
    bl_idname = 'modifiers.noise_preset'
    bl_label = 'Adding a noise modifier, create with a simple displace'
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        selected = context.active_object
        return selected is not None and selected.type == 'MESH'

    def execute(self, context):
        context = context.object
        modifier_name = 'Displace Noise'
        modifier_texture = 'Cloud Displace'

        if modifier_texture not in bpy.data.textures:
            bpy.ops.texture.new()
            bpy.data.textures['Texture'].name = modifier_texture
            bpy.data.textures[modifier_texture].type = 'CLOUDS'

        # Create Noise Modifier
        context.modifiers.new(modifier_name, type='DISPLACE')
        context.modifiers[-1].texture = bpy.data.textures[modifier_texture]
        context.modifiers[-1].strength = .1

        return {'FINISHED'}


class STILO_OT_edge_split_preset(bpy.types.Operator):
    """Add a edge split modifier with a dedicated preset"""
    bl_idname = 'modifiers.edgesplit_preset'
    bl_label = 'Preset to add a Edge Split'
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        selected = context.active_object
        return selected is not None and selected.type == 'MESH'

    def execute(self, context):
        modifier_name = 'EdgeSplit'
        angle = context.scene.md_split.angle_intensity

        md = context.object.modifiers
        md.new(modifier_name, type='EDGE_SPLIT')

        md[-1].split_angle = angle
        if angle == 0:
            md[-1].use_edge_angle = False

        return {'FINISHED'}
