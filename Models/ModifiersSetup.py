import bpy
import math


def udapte_display_md(self, context):
    bpy.ops.modifiers.display_modifiers()
    return


class ModifierSettings(bpy.types.PropertyGroup):
    expand: bpy.props.BoolProperty(
        name="Expand",
        description="Boolean to control on/off expand modifier",
        default=False
    )
    axis_x: bpy.props.BoolProperty(
        name="X Axis",
        description="Value for the X Axis",
        default=True
    )
    axis_y: bpy.props.BoolProperty(
        name="Y Axis",
        description="Value for the Y Axis"
    )
    axis_z: bpy.props.BoolProperty(
        name="Z Axis",
        description="Value for the Z Axis"
    )

    md_render: bpy.props.BoolProperty(
        name="Render Activate",
        description="Setup to hide or rendering modifier",
        default=True,
        update=udapte_display_md,
    )
    md_viewport: bpy.props.BoolProperty(
        name="Viewport Activate",
        description="Show modifier on viewport",
        default=True,
        update=udapte_display_md,
    )
    md_edit: bpy.props.BoolProperty(
        name="Enable on Edit",
        description="Control the modifier on edit mode",
        update=udapte_display_md,
    )
    md_cage: bpy.props.BoolProperty(
        name="Show edit full",
        description="Used a cage or apply on edit mode",
        update=udapte_display_md,
    )


class ModifierSubsurf(bpy.types.PropertyGroup):
    subsurf_intensity: bpy.props.EnumProperty(
        items=[("1X", "1", "", 1), ("2X", "2", "", 2), ("4X", "4", "", 3)],
        name="Subsurf",
        description="Subsurf Intensity",
        default="2X",
    )


class AngleEdge(bpy.types.PropertyGroup):
    angle_intensity: bpy.props.FloatProperty(
        name="Smoothing intensity",
        default=math.radians(45),
        min=0,
        max=math.radians(180),
        subtype='ANGLE',
        )
